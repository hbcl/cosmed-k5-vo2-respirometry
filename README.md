# Cosmed K5 Portable metabolics system
(login username: admin pw: biodex1 or biodex as of 2020-09-28. Please adjust when confirmed)

## When finished experimenting please return:
### 1 Mask, sampling ring, turbine to drying towels
### 2 Laptop, luggage container to cabinet
### 3 Calibration syringe to cabinet
### 4 Straps to cabinet, or left to dry if washed.

## Figure 1 Equipment to calibrate flow
![calibrationFig](assets/image001.jpg)

## Figure 2 Additional Filter:
as of 2024, we need a connecting filter that sits between the Syringe and Syringe fitting (shown in figure 1). We borrow it from the ex phys folks. 
You can find it in the cabinet above the sink in the Fitness Testing & Teaching Lab (Room B102). It is labelled 'Filter' in the following figures. 

## Figure 3 experiment exploded view
![experimentEquipment](assets/image002.jpg)

## Figure 4 experiment assembled view
![experimentAssembled](assets/IMG_9505.jpg)


## Tutorial videos
[1 of 2] (https://youtu.be/5Z-2aUt7JU0)
[2 of 2] (https://youtu.be/8dR5yLVT5sY)
Notes:
- The K5 still displays (as of 2024) the "cal factors out of range, accept calibration?" error. Cosmed
reports that this is caused by Calgary's altitude. It is apparently OK to click the checkmark and continue.
- We use BxB (breath-by-breath) rather than Mix (mixing chamber) because according to Cosmed, Mix simply averages a few breaths. 

## Cleaning instructions
(all cleaning materials are currently (2019-08) found in the treadmill room.

### Masks
After scrubbing with soap and water, rinsing and wiping clean any make-up or debris, please soak in Totalcide for 15 minutes (large stainless container), 
followed by water (small stainless container) for 15 minutes.

### turbine/rings
_WARNING_ Avoid ever pouring streams of water through the turbine! It could break that way and they are expensive!
5% bleach for 10 minutes (glass container); *distilled* water (small stainless container) for 10 minutes. 

## General Maintenance
Straps require washing when they smell. Soaking in soap + water for an hour works reasonably well. 
Please feel free to use a sink for this purpose.

## Set up

•	The sensors are temperature sensitive. Turn the device on 30 minutes before experimenting [ top-right button].

•	Put battery in portable device

•	Attach charging cable, it will click into place when it lines up correctly

•	On the Computer

		K5 connects via Bluetooth
		
		If unresponsive, hold power for 5-7 second
		
		Login, passcode on computer
		
		Open Cosmed Omnia
		
		Test-Cardiopulmonary- BxB, test type- 'Other'
		
•	Attach “flow” (green one that clicks into place) and ‘inlet’ (black and white one that twists on) cables


•	Attach the fan piece or turbine piece to the black piece that is already attached to the cables

		It will slide into place, but some force is needed, don’t touch the fan, press on the edges
		
•	Attach the small black circular piece that has the two-winged looking pieces to the other side of the fan, this piece is where the white cable attaches (the black cable is already attached and stays attached to that first piece we started with)



### Calibrate:

•	The longer black cylinder piece attaches to the fan, the smaller end fits into the calibration tank

•	Press calibration on the device, then press flowmeter

•	Plunges- make sure they are full strokes and you don’t slam it

•	When the device reads “start”, start, when it beeps and reads “complete”, take the black cylinder piece out of the calibration tank and detach it from the fan

•	Room air- choose BxB, just step back and wait for calibration to be done

•	Reference Gas-get reference gas tank from the corner

	o	It will do room air and then beep
	
	o	Attach white pin to the white part of the tank on the side opposite to the container, leave black cord and fan piece in the container
	
	o	Turn black knob CCW 90°
	
	o	It will say Calibration out of range and that is fine, press accept
	
	o	When calibration is complete, take white pin piece out, close gas by turning that knob CW and return tank to the corner


### To attach mask:

•	Use the black piece with the two circular end pieces, the smaller circle fits into the mask where the two blue circles form a crease

•	Then attach the larger of the two circular ends to the fan

•	Black clips on head piece clip into blue mask (4 clips)

•	Larger circle of the head piece should be above the smaller circle, the smaller circular opening of head piece should rest near the neck

•	Bottom of K5 device screws in to the backpack and cabels can be clipped in on sholder strap so they are out of the way





